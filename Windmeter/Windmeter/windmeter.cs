﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Devices.Sensors;

using System.ComponentModel;

using System.Windows.Media;

using DSP;

using System.Windows.Threading;

namespace Windmeter
{

    class WindMeterViewModel : INotifyPropertyChanged
    {

        WindMeter windmeter;

        DispatcherTimer dispatcherTimer;

        double windspeed;
        double[] windspeed_history;

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public void ChangeWindMeter(ref WindMeter windmeter_)
        {
            windmeter = windmeter_;
            windspeed_history = windmeter.WindSpeedHistory;
        }

        public WindMeterViewModel(ref WindMeter windmeter_)
        {
            ChangeWindMeter(ref windmeter_);

            //  DispatcherTimer setup
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 125);
            dispatcherTimer.Start();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            windspeed = windmeter.WindSpeed;
            windspeed_history = windmeter.WindSpeedHistory;
            NotifyPropertyChanged("WindSpeed");
            NotifyPropertyChanged("WindSpeedMean");
            NotifyPropertyChanged("WindSpeedMax");
            NotifyPropertyChanged("WindSpeedMin");
            NotifyPropertyChanged("WindSpeedHistory");
        }

        public string WindSpeed 
        {
            get 
            {
                if (windspeed > 0)
                    return windspeed.ToString("n1");
                else
                    return "---";
            }
        }

        public PointCollection WindSpeedHistory
        {
            get 
            {
                PointCollection points = new PointCollection();
                for (var i = 0; i < windspeed_history.Length; i++)
                {
                    System.Windows.Point point = new System.Windows.Point(i, windspeed_history[i]);
                    points.Add(point);
                }

                return points;
            }
        }

        public string WindSpeedMean
        {
            get { return windspeed_history.Average().ToString("n1"); }
        }

        public string WindSpeedMax
        {
            get { return windspeed_history.Max().ToString("n1"); }
        }

        public string WindSpeedMin
        {
            get { return windspeed_history.Min().ToString("n1"); }
        }


        public string Unit
        {
            get { return windmeter.Unit; }
        }

        public string Status
        {
            get { return windmeter.Status; }
        }

        public string Type
        {
            get { return windmeter.Type; }
        }


    }

    abstract class WindMeter
    {
        abstract public double[] WindSpeedHistory { get; }
        abstract public double WindSpeed { get; }
        abstract public string Unit { get; }
        abstract public string Type { get; }
        abstract public string Status { get; }
        abstract public void Start();
        abstract public void Stop();
    }

    class VaavudMeter : WindMeter
    {
        Compass compass = null;

        float fs = 0;
        uint window_size = 16;
        uint pad_factor = 8; // lazy man's interpolation

        uint buffered_data = 0;
        double[] in_r;

        // temp. buffers for calculations
        double[] in_c;
        double[] out_r;
        double[] out_c;
        double[] power;
        uint peak_index = 0;

        double lowpass_fc = 1.5;
        double amplitude_th = 100;
        double hz = -1;
        double rpm_to_ms = 1; // TODO, from vaavud guys ...
        string unit = "Hz";
        string type = "Vaavud";
        string status = "";
        int windspeed_history_length = 30;
        double[] windspeed_history;

        public override double[] WindSpeedHistory { get { return windspeed_history; } }
        
        public override double WindSpeed { get { return hz; } }
                
        public override string Unit { get { return unit; } }
        
        public override string Type { get { return type; } }
        
        public override string Status { get { return status; } }

        public VaavudMeter()
        {

            if (!Compass.IsSupported)
            {
                status = "ERROR: no compass found!";
                return;
            }

            compass = new Compass();
            compass.TimeBetweenUpdates = new TimeSpan(0, 0, 0, 0, 1); // as short as we can
            TimeSpan actual_timespan = compass.TimeBetweenUpdates;
            fs = 1000 / actual_timespan.Milliseconds;

            in_r = new double[pad_factor * window_size];
            in_c = new double[pad_factor * window_size];
            out_r = new double[pad_factor * window_size];
            out_c = new double[pad_factor * window_size];
            power = new double[pad_factor * window_size];

            windspeed_history = new double[windspeed_history_length];

            hz = -2;

//            System.Diagnostics.Debug.WriteLine("fs: " + fs + " Hz");

        }

        private void CollectData(object sender, SensorReadingEventArgs<CompassReading> a)
        {

//            System.Diagnostics.Debug.WriteLine("X " + a.SensorReading.MagnetometerReading.X);
//            System.Diagnostics.Debug.WriteLine("Y " + a.SensorReading.MagnetometerReading.Y);
//            System.Diagnostics.Debug.WriteLine("Z " + a.SensorReading.MagnetometerReading.Z);

            double data_in = a.SensorReading.MagnetometerReading.X + a.SensorReading.MagnetometerReading.Y + a.SensorReading.MagnetometerReading.Z; 

            if (buffered_data < window_size)
            {
                in_r[buffered_data] = data_in;
                buffered_data++;
            }
            
            if (buffered_data >= window_size)
            {
                DSP.FourierTransform.Compute(pad_factor * window_size, in_r, null, out_r, out_c, false);
                DSP.FourierTransform.Norm(pad_factor * window_size, out_r, out_c, power);

                // smooth high pass filter away the DC
                double num_bins = pad_factor * window_size;
                double fraction = (2 * lowpass_fc / fs);
                int dc_block = (int)( fraction * num_bins ) + 1;
                double factor = 0;
                
                for (int i = 0; i <= dc_block; i++)
                {
                    factor = (i / dc_block);
                    factor *= factor;
                    power[i] *= factor;
                    power[i] *= factor;
                    power[i] *= factor;
                    power[pad_factor * window_size - 1 - i] *= factor;
                }

                double tmp_rpm = DSP.FourierTransform.PeakFrequency(pad_factor * window_size, power, fs, ref peak_index);

                // ignore the peak if it's too small
                if (power[peak_index] > amplitude_th) 
                {
                    hz = tmp_rpm;
                    Array.Copy(windspeed_history, 0, windspeed_history, 1, windspeed_history.Length - 1);
                    windspeed_history[0] = hz;
                }
                else
                {
                    hz = -1;
                }

                buffered_data = 0;

//                System.Diagnostics.Debug.WriteLine("rps: " + hz + " Hz ( i " + peak_index + " a " + power[peak_index] + ")");

            }
        }

        public override void Start()
        {

            if (!Compass.IsSupported || compass == null)
                return;

            buffered_data = 0;
            compass.CurrentValueChanged += CollectData;
            compass.Start();

        }

        public override void Stop()
        {

            if (!Compass.IsSupported || compass == null)
                return;

            compass.Stop();
            compass.CurrentValueChanged -= CollectData;

        }

    }
}
