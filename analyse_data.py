X = []
Y = []
Z = []

datafile = open("data.txt", "r")

Fs = None

for line in datafile:
    splt = line.replace(",",".").split(" ")
    
    if splt[0] == "X":
        X.append(float(splt[1]))

    elif splt[0] == "Y":
        Y.append(float(splt[1]))

    elif splt[0] == "Z":
        Z.append(float(splt[1]))

    elif splt[0] == "fs:":
        Fs = float(splt[1])

import numpy
import scipy
import scipy.signal

X = numpy.array(X)
Y = numpy.array(Y)
Z = numpy.array(Z)

l = min(map(len, [X,Y,Z]))

X = X[0:l]
Y = Y[0:l]
Z = Z[0:l]

V = X + Y + Z

h=scipy.signal.firwin( numtaps=31, cutoff=1.5, nyq=Fs/2, pass_zero=False) # do a IIR filter instead?
V_lp = scipy.signal.lfilter( h, 1.0, V)

fftsz = 16

V_lp_pad = numpy.zeros(len(V_lp) + (fftsz - len(V_lp) % fftsz))
V_lp_pad[0:len(V_lp)] = V_lp
fold = V_lp_pad.reshape(-1, fftsz)

pad_f = 8;

fold_pad = numpy.zeros( (fold.shape[0], pad_f * fold.shape[1]) )
fold_pad[:,:fold.shape[1]] = fold[:,:]

fold_power = abs(numpy.fft.fft(fold_pad))[:,:pad_f * fftsz / 2.0]

bins = numpy.argmax(fold_power, axis=1)
values = numpy.max(fold_power, axis=1)

value_th = 2 # not universially true I guess but seems to be good
valid_bins = numpy.argwhere(values > value_th)
invalid_bins = numpy.argwhere(values <= value_th)

def find_local_extremas(x, hysteresis):

    last_maxima = 0
    last_minima = 0

    peaks = []
    valleys = []
    extremas = []

    state = "look for peak"

    for i in range(len(x)):
        if state == "look for peak":
            if x[i] < x[i-1] and x[i-1] > (last_minima + hysteresis):
                extremas.append(i-1)
                peaks.append(i-1)
                last_maxima = x[i-1]
                state = "look for valley"

        elif state == "look for valley":
            if x[i] > x[i-1] and x[i-1] < (last_maxima - hysteresis):
                extremas.append(i-1)
                valleys.append(i-1)
                last_minima = x[i-1]
                state = "look for peak"

    ln = min(len(peaks), len(valleys))

    return numpy.array(peaks[:ln]), numpy.array(valleys[:ln]), numpy.array(extremas)

hysteresis_th = 0.01
peak_ids, valley_ids, extremas_ids = find_local_extremas(V_lp, hysteresis_th)

peak_periods = numpy.diff(peak_ids)
valley_periods = numpy.diff(valley_ids)
extremas_halfperiods = numpy.diff(extremas_ids)

import pylab

T_max = len(V_lp) / float(Fs)
T = numpy.array(range(len(V_lp))) / float(Fs)

pylab.subplot(4, 1, 1)

pylab.suptitle("Comparison of frequency tracking methods")

pylab.title('Raw data X Y Z') 
pylab.plot(T, X); pylab.plot(T, Y); pylab.plot(T, Z)
pylab.legend(["X", "Y", "Z"], fontsize=8)
pylab.ylabel("Amplitude")

pylab.subplot(4, 1, 2)
pylab.title('V = LP(X+Y+Z)') 
pylab.plot(T, V_lp)
pylab.plot(peak_ids / float(Fs), V_lp[peak_ids], ".")
pylab.plot(valley_ids / float(Fs), V_lp[valley_ids], ".")
pylab.legend(["V = LP(X+Y+Z)","Local max","Local min"], fontsize=8)
pylab.ylabel("Amplitude")

pylab.subplot(4, 1, 3)
pylab.title('Spectrogram peaks of V') 
pylab.imshow(fold_power.T, aspect="auto", cmap=pylab.cm.gist_heat, origin="lower", extent=[0, T_max, 0, Fs/2])
T_mini = T_max * numpy.linspace(0, 1, len(bins))
yf = Fs / float(fftsz * pad_f)
pylab.plot(T_mini, yf * bins, "--")
pylab.plot(T_mini[valid_bins], yf * bins[valid_bins], ".")
pylab.plot(T_mini[invalid_bins], yf * bins[invalid_bins], "x")
pylab.legend(["peak trace","kept", "discarded"], fontsize=8)
pylab.ylabel("Frequency [Hz]")

pylab.subplot(4, 1, 4)
pylab.title('Local extrema frequency of V') 
pylab.imshow(fold_power.T, aspect="auto", cmap=pylab.cm.gist_heat, origin="lower", extent=[0, T_max, 0, Fs/2])
yf = Fs
pylab.plot(peak_ids[:-1] / float(Fs), 1.0 / peak_periods * yf, "--")
pylab.plot(valley_ids[:-1] / float(Fs), 1.0 / valley_periods * yf, "--")
pylab.legend(["using maxima","using minima"], fontsize=8)
pylab.ylabel("Frequency [Hz]")

pylab.xlabel("Time [s]")

pylab.show()
